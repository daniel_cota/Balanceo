# Práctica | Balanceo de cargas

Práctica de la clase utilizando Docker Compose y aplicación brindada por el profesor [APP](https://gitlab.com/luis190991/balancingapp)


# ¿Cómo funciona?

Para poder ejecutar este proyecto solo debemos relaizar los siguientes pasos:
1. Clonar el repositorio.
2. Ejecutar: docker-compose up
3. Acceder al puerto 3000, 3001 o 30002

# Autor
	329701 - Daniel Cota